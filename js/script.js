// Для роботи з input не рекомендується використовувати клавіатуру в контексті деяких робототехнічних задач з 
// двох причин:

// Обмежений спектр можливих вхідних даних: Клавіатура зазвичай дозволяє вводити лише текстові дані,
//  числа та деякі спеціальні символи. Це обмежує можливості введення складніших або структурованих даних,
//  таких як зображення, звук, відео або сенсорні дані. Для таких задач існують інші типи пристроїв вводу, 
//  такі як камери, мікрофони, сенсори дотику та жестів, які дозволяють отримати більш розширений набір вхідних даних.

// Неефективність та неадаптованість: Використання клавіатури може бути незручним для багатьох робототехнічних задач, 
// особливо якщо робот повинен взаємодіяти з людьми або виконувати фізичні дії. 
// У таких випадках може бути важко адаптувати клавіатуру до потреб конкретної задачі,
//  і використання інших пристроїв вводу, які краще відповідають вимогам задачі, може бути більш доцільним.


let buttons = document.querySelectorAll('.btn')
document.addEventListener('keydown', buttonColor)

function buttonColor(event){
    if(event.code){
      for(let i=0; i<buttons.length; i++){
        if(buttons[i].classList.contains(event.code)){
            buttons.forEach(element => {
                element.classList.remove('blue')
            });
            buttons[i].classList.add('blue')
        }
      }
    }}

  